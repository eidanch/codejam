__author__ = 'eidanch'

from itertools import combinations


def get_line():
    return raw_input().strip()

formatIntegerList = lambda s: list(map(int, s.split(' ')))


def standard_input():
    T = int(get_line())
    for i in range(T):
        R, C, N = tuple(formatIntegerList(get_line()))
        yield (i+1, (R, C, N))


def handle_case(case):
    R, C, N = case
    R, C = max(R, C), min(R, C)
    lst = [(i, j) for i in xrange(R) for j in xrange(C) if (i + j) % 2 == 0]
    lst += [(i,j) for i,j in [(0,C-1)] if (i+j) % 2 == 1 and (i,j) not in lst]
    lst += [(i,j) for i,j in [(R-1,0)] if (i+j) % 2 == 1 and (i,j) not in lst]
    lst += [(i,j) for i,j in [(R-1,C-1)] if (i+j) % 2 == 1 and (i,j) not in lst]
    lst += [(i, 0)for i in xrange(R) if i % 2 == 1 and (i,0) not in lst]
    lst += [(0, j)for j in xrange(C) if j % 2 == 1 and (0,j) not in lst]
    lst += [(i, C - 1)for i in xrange(1, R) if (i + C - 1) % 2 == 1 and (i,C-1) not in lst]
    lst += [(R - 1, j)for j in xrange(1, C) if (j + R - 1) % 2 == 1 and (R-1,j) not in lst]
    lst += [(i, j) for i in xrange(1, R - 1) for j in xrange(1, C - 1) if (i + j) % 2 == 1 and (i,j) not in lst]
    #print len(lst)
    assert set(lst) == set((i, j) for i in xrange(R) for j in xrange(C))
    #print lst[:N]
    return unhappiness(lst[:N])


def brute_force(case):
    R, C, N = case
    lst = [(i,j) for i in xrange(R) for j in xrange(C)]
    best = 4*R*C
    for occupied in combinations(lst, N):
        value = unhappiness(occupied)
        best = min(best, value)
        if best == 0:
            return 0
    return best


def unhappiness(occupied):
    count = 0
    for (i, j) in occupied:
        if (i - 1, j) in occupied:
            count += 1
        if (i, j - 1) in occupied:
            count += 1
        if (i + 1, j) in occupied:
            count += 1
        if (i, j + 1) in occupied:
            count += 1
    return count / 2

def main():
    for i, case in standard_input():
        print "Case #%d: %d" % (i, handle_case(case))

if __name__ == '__main__':
    main()