# -*- coding: utf-8 -*-
"""
Created on Sat Apr 16 10:22:36 2016

@author: Eidan Cohen
"""

def solve(s):
    word = s[0]
    for c in s[1:]:
        if ord(c) >= ord(word[0]):
            word = c + word
        else:
            word = word + c
    return word
    
def main():
    T = int(input())
    for i in range(1,T+1):
        s = input()
        print('Case #{0}: {1}'.format(i, solve(s)))

if __name__ == '__main__':
    main()

