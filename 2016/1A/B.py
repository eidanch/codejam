# -*- coding: utf-8 -*-
"""
Created on Sat Apr 16 10:33:01 2016

@author: Eidan Cohen
"""

"""
1
3
1 2 3
2 3 5
3 5 6
2 3 4
1 2 3

"""

def sub_row(matrix, row, length):
    return [matrix[row][j] for j in range(length)]
    
def sub_column(matrix, column, length):
    return [matrix[j][column] for j in range(length)]

def validate_row(n, result, lists, lst, i):
    for j in range(i):
        if result[i][j] > 0 and result[i][j] != lst[j]:
            return False
    lst_prefixes = [l[:i+1] for l in lists]
    return all(sub_column(result, j, i) + [lst[j]] in lst_prefixes for j in range(i+1,n))
    
def validate_column(n, result, lists, lst, i):
    for j in range(i):
        if result[j][i] > 0 and result[j][i] != lst[j]:
            return False
    lst_prefixes = [l[:i+1] for l in lists]
    return all(sub_row(result, j, i) + [lst[j]] in lst_prefixes for j in range(i+1,n))

def solve(n, lists):
    result = [n*[0] for i in range(n)]
    missing_index, is_missing_row = None, None
    for i in range(n):
        d = min(lst[i] for lst in lists)
        curr_lists = [lst for lst in lists if lst[i] == d]
        row_filled, column_filled = False, False
        print(result)
        for lst in curr_lists:
            print(i, lst)
            lists.remove(lst)
            if not row_filled and validate_row(n, result, lists, lst, i):
                print('row', i, lst)
                row_filled = True
                for j in range(n):
                    result[i][j] = lst[j]
            elif not column_filled and validate_column(n, result, lists, lst, i):
                print('col', i, lst)
                column_filled = True
                for j in range(n):
                    result[j][i] = lst[j]
            else:
                assert False
        if not row_filled:
            missing_index = i
            is_missing_row = True
        if not column_filled:
            missing_index = i
            is_missing_row = False
    print(result)
    if is_missing_row:
        return [result[missing_index][j] for j in range(n)]
    else:
        return [result[j][missing_index] for j in range(n)]
            
    
def main():
    T = int(input())
    for i in range(1,T+1):
        n = int(input())
        lists = []
        for j in range(2*n-1):
            line = input()
            lists.append([int(r) for r in line.strip().split(' ')])
        print('Case #{0}: {1}'.format(i, solve(n, lists)))

if __name__ == '__main__':
    main()