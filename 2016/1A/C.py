# -*- coding: utf-8 -*-
"""
Created on Sat Apr 16 11:26:33 2016

@author: Eidan Cohen
"""

from collections import deque

NEG_INFINITY = -1000000

def find_path(graph, src, dst):
    closed = set()
    opened = deque()
    opened.append((src, 0))
    while len(opened) > 0:
        current, length = opened.popleft()
        if current == dst:
            return length
        if current in closed:
            continue
        for nxt in graph[current]:
            opened.append((nxt, length + 1))
        closed.add(current)
    return NEG_INFINITY

def solve(lst):
    best_circle = 0    
    for i in range(len(lst)):
        graph = {}
        for j in range(len(lst)):
            graph[j] = set()
        for u,v in enumerate(lst):
            v -= 1
            if u != i:
                graph[u].add(v)
                graph[v].add(u)
        u,v = i, lst[i] - 1
        print(u,v,graph, find_path(graph, u, v))
        best_circle = max(best_circle, 1 + find_path(graph, u, v))
    return best_circle
    
def main():
    T = int(input())
    for i in range(1,T+1):
        n = int(input())
        line = input()
        lst = [int(r) for r in line.strip().split(' ')]
        print('Case #{0}: {1}'.format(i, solve(lst)))

if __name__ == '__main__':
    pass #main()

