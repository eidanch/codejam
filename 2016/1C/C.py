# -*- coding: utf-8 -*-
"""
Created on Sun May  8 12:36:45 2016

@author: Eidan Cohen
"""

from itertools import combinations

def mem_handle(mem, a, b , c, k):
    r1, r2, r3 = mem[0].get((a,b), 0), mem[1].get((b,c), 0), mem[2].get((a,c), 0)
    if r1 == k or r2 == k or r3 == k:
        return False
    mem[0][(a,b)] = r1 + 1
    mem[1][(b,c)] = r2 + 1
    mem[2][(a,c)] = r3 + 1
    return True

def solve(j,p,s,k):
    result = ""
    count = 0
    mem = [dict(), dict(), dict()]
    for a in range(j):
        for b in range(p):
            for c in range(s):
                if mem_handle(mem,a,b,c, k):
                    count += 1
                    result += "\n{0} {1} {2}".format(a+1,b+1,c+1)
    return str(count) + result

def create_matrix(m,n):
    return [n*[0] for i in range(m)]

def validate(rs, j,p,s,k):
    m1 = create_matrix(j, p)
    m2 = create_matrix(p, s)
    m3 = create_matrix(j, s)
    for (a,b,c) in rs:
        m1[a][b] += 1
        m2[b][c] += 1
        m3[a][c] += 1
        if m1[a][b] > k or m2[b][c] > k or m3[a][c] > k:
            return False
    return True

def brute_solve(j,p,s,k):
    k = min(s,k)
    domain = []    
    for a in range(j):
        for b in range(p):
            for c in range(s):
                domain.append((a,b,c))
    m = j*p*s
    for count in range(m, 0, -1):
        for rs in combinations(domain, count):
            if validate(rs, j,p,s,k):
                return str(count) + '\n' + '\n'.join(' '.join(str(i+1) for i in x) for x in rs)
    assert False
    
def main():
    t = int(input())
    for i in range(1,t+1):
        j,p,s,k = tuple(int(x) for x in input().split(" "))
        print('Case #{0}: {1}'.format(i, brute_solve(j,p,s,k)))

if __name__ == '__main__':
    main()
