

def solve(ponies):
    for index in range(6):
        if ponies[index + 1] == 0:
            continue
        result = solve2(ponies, index)
        if result != "IMPOSSIBLE":
            return result
    #assert any(x > sum(ponies[1:]) - x for x in ponies[1:])
    return "IMPOSSIBLE"

def solve2(ponies, current=None):
    _, red, orange, yellow, green, blue, violet = ponies
    #_, red, _, yellow, _, blue, _ = ponies
    colors = [red, orange, yellow, green, blue, violet]
    letters = ["R", "O", "Y", "G", "B", "V"]
    neighbors = [(0,1,5), (0,1,2), (1,2,3), (2,3,4), (3,4,5), (4,5,0)]
    result = letters[current]
    colors[current] -= 1
    while max(colors) > 0:
        sorted_colors = sorted(list(range(6)), key=lambda index: colors[index], reverse=True)
        for invalid_index in neighbors[current]:
            sorted_colors.remove(invalid_index)
        current = sorted_colors[0]
        if colors[current] == 0:
            return "IMPOSSIBLE"
        colors[current] -= 1
        result += letters[current]
    if result[0] == result[-1]:
        return "IMPOSSIBLE"
    return result

def main():
    cases = int(input())
    for case in range(1, cases+1):
        ponies = tuple(int(x) for x in input().rstrip().split(" "))
        print("Case #%d: %s" % (case, solve(ponies)))

if __name__ == '__main__':
    main()
    #print(solve((36, 17, 0, 14, 0, 5, 0)))
