
def is_tidy(n):
    sn = str(n)
    return ''.join(sorted(sn)) == sn

def find(sbound):
    first = int(sbound[0])
    first_low_index = 0
    while first_low_index < len(sbound) and first <= int(sbound[first_low_index]):
        first_low_index += 1
    last_high_index = first_low_index - 1
    while last_high_index > 0 and first == int(sbound[last_high_index]):
        last_high_index -= 1
    return first_low_index, last_high_index

def solve_rec(sbound, went_under=False):
    if len(sbound) == 0:
        return ""
    if went_under:
        return '9'*len(sbound)
    current_bound = min(int(digit) for digit in sbound)
    went_under = int(sbound[0]) > current_bound
    if went_under:
        current_digit = int(sbound[0]) - 1
    else:
        current_digit = int(sbound[0])
    return str(current_digit) + solve_rec(sbound[1:], went_under)

def solve(bound):
    sbound = str(bound)
    first_low_index, last_high_index = find(sbound)
    if first_low_index < len(sbound):
        return solve_rec(sbound[:last_high_index] + str(int(sbound[last_high_index]) - 1) + '9'*(len(sbound) - last_high_index - 1))
    return solve_rec(sbound)

def main():
    cases = int(input())
    for i in range(1, cases+1):
        bound = int(input())
        print("Case #%d: %s" % (i, int(solve(bound))))

if __name__ == '__main__':
    main()
    #print(find('795'))
    #print(solve(795))
