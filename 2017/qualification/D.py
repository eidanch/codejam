

def update(row, col, model, length, rows, cols, diags, rev_diags):
    if model == '.':
        return
    if model != '+':
        rows[row] = False
        cols[col] = False
    if model != 'x':
        diags[row + col] = False
        rev_diags[row - col + length] = False

def constraints(grid):
    length = len(grid)
    rows = [True]*length
    cols = [True]*length
    diags = [True]*(2*length)
    rev_diags = [True]*(2*length)
    for row in range(length):
        for col in range(length):
            update(row, col, grid[row][col], length, rows, cols, diags, rev_diags)
    return rows, cols, diags, rev_diags

def score(grid):
    length = len(grid)
    result = 0
    for row in range(length):
        for col in range(length):
            if grid[row][col] == '+':
                result += 1
            if grid[row][col] == 'x':
                result += 1
            if grid[row][col] == 'o':
                result += 2
    return result


def solve_greedy(grid):
    length = len(grid)
    rows, cols, diags, rev_diags = constraints(grid)
    #handle xs
    moves = []
    for row in range(length):
        if not rows[row]:
            continue
        for col in range(length):
            if cols[col] and grid[row][col] == '.':
                moves.append(('x', row, col))
                grid[row][col] = 'x'
                update(row, col, grid[row][col], length, rows, cols, diags, rev_diags)
                break
    # Handle +s
    for row in range(length):
        for col in range(length):
            if grid[row][col] == '.' and all([diags[row + col], rev_diags[row - col + length]]):
                moves.append(('+', row, col))
                grid[row][col] = '+'
                update(row, col, grid[row][col], length, rows, cols, diags, rev_diags)
    
    for row in range(length):
        for col in range(length):
            if grid[row][col] == '.' and all([rows[row], cols[col], diags[row + col], rev_diags[row - col + length]]) or \
                grid[row][col] == '+' and all([rows[row], cols[col]]) or \
                grid[row][col] == 'x' and all([diags[row + col], rev_diags[row - col + length]]):

                moves.append(('o', row, col))
                grid[row][col] = 'o'
                update(row, col, grid[row][col], length, rows, cols, diags, rev_diags)
    #print(grid)
    return score(grid), moves

def solve_backtrack(grid):
    length = len(grid)
    rows, cols, diags, rev_diags = constraints(grid)
    moves = []
    for row in range(length):
        for col in range(length):
            if grid[row][col] == '.':
                if rows[row] and cols[col]:
                    move = (1, row, col, 'x')
                    moves.append(move)
                if diags[row + col] and rev_diags[row - col + length]:
                    move = (1, row, col, '+')
                    moves.append(move)
                if all([rows[row], cols[col], diags[row + col], rev_diags[row - col + length]]):
                    move = (2, row, col, 'o')
                    moves.append(move)
            elif grid[row][col] == '+':
                if rows[row] and cols[col]:
                    move = (1, row, col, 'o')
                    moves.append(move)
            elif grid[row][col] == 'x':
                if diags[row + col] and rev_diags[row - col + length]:
                    move = (1, row, col, 'o')
                    moves.append(move)
    if len(moves) == 0:
        #if score(grid) == 10 and 'x' in grid[0]:
        #    print(grid)
        return (score(grid), [])
    result = (0, [])
    for move in moves:
        _, row, col, model = move
        original = grid[row][col]
        grid[row][col] = model
        rec_score, rec_moves = solve_backtrack(grid)
        result = max(result,(rec_score, [(row, col, model)] + rec_moves))
        grid[row][col] = original

    return result


def empty_grid(length):
    def empty_row(length):
        return length*['.']
    grid = [empty_row(length) for i in range(length)]
    return grid

    
def grid_from_row(row):
    length = len(row)
    def empty_row(length):
        return length*['.']
    grid = [row] + [empty_row(length) for i in range(length - 1)]
    return grid


def main():
    cases = int(input())
    for i in range(1, cases+1):
        length, models = tuple(int(x) for x in input().rstrip().split(" "))
        grid = empty_grid(length)
        for _ in range(models):
            model, row, col = input().rstrip().split(" ")
            row, col = int(row) - 1, int(col) - 1
            grid[row][col] = model
        grid_score, moves = solve_greedy(grid)
        print("Case #%d: %d %d" % (i, grid_score, len(moves)))
        used = set()
        for model, row, col in reversed(moves):
            if (row, col) in used:
                continue
            print(model, row + 1, col + 1)
            used.add((row, col))

if __name__ == '__main__':
    main()
    #print(solve_backtrack(grid_from_row(['+','.','x','.'])))
    #print(solve_greedy(grid_from_row(['+','.','x','.'])))