from sys import stdout, stderr

def guess(A, B):
    Q = (A + B) // 2
    print(Q)
    stdout.flush()
    response = input()
    if response == "TOO_SMALL":
        A = Q + 1
    elif response == "TOO_BIG":
        B = Q - 1
    return A, B, response

def handle_case():
    A, B = tuple(int(x) for x in input().split(' '))
    A += 1
    N = int(input())
    response = None
    while response not in ["CORRECT", "WRONG_ANSWER"]:
        A, B, response = guess(A,B)

T = int(input())
for case_id in range(T):
    handle_case()