
from string import ascii_uppercase

def evacuate(parties):
    result = []
    while sum(parties.values()) > 0:
        e1 = max(parties.keys(), key=parties.get)
        parties[e1] -= 1
        if max(parties.values()) > sum(parties.values()) / 2:
            e2 = max(parties.keys(), key=parties.get)
            parties[e2] -= 1
        else:
            e2 = ''
        result.append(e1 + e2)
    return result

def handle_case(case_id):
    N = int(input())
    P = [int(x) for x in input().split(' ')]
    parties = {}
    for i in range(len(P)):
        parties[ascii_uppercase[i]] = P[i]

    result = evacuate(parties)
    print("Case #{}: {}".format(case_id, ' '.join(result)))

T = int(input())
for case_id in range(T):
    handle_case(case_id + 1)