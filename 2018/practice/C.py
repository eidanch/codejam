
def calc(D, horses):
    result = None
    for K, S in horses:
        time = (D - K) / S
        speed = D / time
        if result is None:
            result = speed
        else:
            result = min(result, speed)
    return result

def handle_case(case_id):
    D,N = tuple(int(x) for x in input().split(' '))
    horses = []
    for i in range(N):
        K, S = tuple(int(x) for x in input().split(' '))
        horses.append((K,S))

    result = calc(D, horses)
    print("Case #{}: {:.6f}".format(case_id, result))

T = int(input())
for case_id in range(T):
    handle_case(case_id + 1)