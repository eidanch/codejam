from math import floor, ceil
from collections import defaultdict

def divide(x):
    half = (x - 1)/2
    return ceil(half), floor(half)

def calc(N, K):
    counts = defaultdict(lambda: 0)
    counts[N] += 1
    while K > 0:
        m = max(counts)
        c = counts[m]
        r,l  = divide(m)
        if c >= K:
            return max(l,r), min(l,r)
        K -= c
        counts[r] += c
        counts[l] += c
        del counts[m]
    return None

def handle_case(case_id):
    N, K = tuple(int(x) for x in input().split(' '))
    result = calc(N, K)
    print("Case #{}: {}".format(case_id, ' '.join(map(str, result))))

T = int(input())
for case_id in range(T):
    handle_case(case_id + 1)