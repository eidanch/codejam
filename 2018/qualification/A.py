from collections import namedtuple

class InputVars(object):
    def __init__(self, names, dtypes=None):
        self.names = names
        if dtypes is None:
            dtypes = len(names)*[int]
        self.dtypes = dtypes
    
    def __call__(self, vars):
        s = input()
        result = {}
        for name, dtype, sval in zip(self.names, self.dtypes, s.split(' ')):
            result[name] = dtype(sval)
        return result

class InputList(object):
    def __init__(self, name, dtype=None):
        self.name = name
        if dtype is None:
            dtype = int
        self.dtype = dtype
    
    def __call__(self, vars):
        s = input()
        return { self.name: [self.dtype(w) for w in s.split(' ')] }

class InputLines(object):
    def __init__(self, name, count_var, line_input):
        self.name = name
        self.count_var = count_var
        self.line_input = line_input
    
    def __call__(self, vars):
        count = vars[self.count_var]
        result = []
        for i in range(count):
            d = self.line_input(vars)
            value = namedtuple("line", d.keys())(**d)
            result.append(value)
        return { self.name: result }

class CodeJamSolver(object):
    def __init__(self, inputs, solve, output_formatter=None):
        self.inputs = inputs
        self.solve = solve
        if output_formatter is None:
            output_formatter = str
        self.output_formatter = output_formatter

    def handle_inputs(self):
        vars = {}
        for inp in self.inputs:
            vars.update(inp(vars))
        return vars

    def handle_case(self, case_id):
        vars = self.handle_inputs()
        result = self.solve(**vars)
        print("Case #{}: {}".format(case_id, self.output_formatter(result)))

    def main(self):
        T = int(input())
        for case_id in range(1, T + 1):
            self.handle_case(case_id)

    def __call__(self):
        self.main()

list_formatter = lambda lst: ' '.join(str(x) for x in lst)

##########################
#       Solve Area       #
##########################

A = [
    InputVars(['D', 'P'], [int, str])
]

def total_damage(P):
    total = 0
    current = 1
    for instruction in P:
        if instruction == 'S':
            total += current
        elif instruction == 'C':
            current *= 2
    return total

def solveA(D, P):
    if P.count('S') > D:
        return "IMPOSSIBLE"
    current = len(P) - 1
    count = 0
    while total_damage(P) > D:
        while P[current - 1:current + 1] != 'CS':
            current -= 1
        P = P[:current - 1] + 'SC' + P[current + 1:]
        current += 1
        count += 1
    return count
    

CodeJamSolver(A, solveA).main()