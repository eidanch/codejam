
from collections import namedtuple

class InputVars(object):
    def __init__(self, names, dtypes=None):
        self.names = names
        if dtypes is None:
            dtypes = len(names)*[int]
        self.dtypes = dtypes
    
    def __call__(self, vars):
        s = input()
        result = {}
        for name, dtype, sval in zip(self.names, self.dtypes, s.split(' ')):
            result[name] = dtype(sval)
        return result

class InputList(object):
    def __init__(self, name, dtype=None):
        self.name = name
        if dtype is None:
            dtype = int
        self.dtype = dtype
    
    def __call__(self, vars):
        s = input()
        return { self.name: [self.dtype(w) for w in s.split(' ')] }

class InputLines(object):
    def __init__(self, name, count_var, line_input):
        self.name = name
        self.count_var = count_var
        self.line_input = line_input
    
    def __call__(self, vars):
        count = vars[self.count_var]
        result = []
        for i in range(count):
            d = self.line_input(vars)
            value = namedtuple("line", d.keys())(**d)
            result.append(value)
        return { self.name: result }

class CodeJamSolver(object):
    def __init__(self, inputs, solve, output_formatter=None):
        self.inputs = inputs
        self.solve = solve
        if output_formatter is None:
            output_formatter = str
        self.output_formatter = output_formatter

    def handle_inputs(self):
        vars = {}
        for inp in self.inputs:
            vars.update(inp(vars))
        return vars

    def handle_case(self, case_id):
        vars = self.handle_inputs()
        result = self.solve(**vars)
        print("Case #{}: {}".format(case_id, self.output_formatter(result)))

    def main(self):
        T = int(input())
        for case_id in range(1, T + 1):
            self.handle_case(case_id)

    def __call__(self):
        self.main()

list_formatter = lambda lst: ' '.join(str(x) for x in lst)

##########################
#       Solve Area       #
##########################

B = [
    InputVars(['N']),
    InputList('V')
]

def trouble_sort_efficient(lst):
    tt = [sorted(lst[::2]), sorted(lst[1::2])]
    result = []
    for i in range(len(lst)):
        result.append(tt[i % 2][i // 2])
    return result

def solveB(N, V):
    ts = trouble_sort_efficient(V)
    for i in range(len(ts) - 1):
        if ts[i] > ts[i+1]:
            return i
    return "OK"

CodeJamSolver(B, solveB).main()

def trouble_sort(lst):
    done = False
    while not done:
        done = True
        for i in range(0, len(lst) - 2):
            if (lst[i] > lst[i + 2]):
                done = False
                temp = lst[i + 2]
                lst[i + 2] = lst[i]
                lst[i] = temp
    return lst


def generate_list(n):
    import random
    lst = []
    for i in range(n):
        lst.append(random.randint(0,1000))
    return lst

def test():
    for i in range(100):
        lst = generate_list(10000)
        solveB(len(lst), lst)
        #assert(trouble_sort_efficient(lst) == trouble_sort(lst))
