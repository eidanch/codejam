from math import sqrt, ceil
from sys import stderr, stdout

def create_matrix(a,b):
    return [(lambda: b*[0])() for i in range(a)]

def score_point(matrix, row, col):
    return sum(matrix[i][j] for i in range(row-1, row+2) for j in range(col-1, col+2))

def score_matrix(matrix):
    return sum(sum(row) for row in matrix)

def best_point(matrix, a, b):
    best_score = 9
    result = (1, 1)
    for i in range(1, a - 1):
        for j in range(1, b - 1):
            score = score_point(matrix, i, j)
            if score < best_score:
                best_score = score
                result = i, j
    return result

def handle_case(A):
    a = ceil(sqrt(A))
    b = a
    while a*(b - 1) >= A:
        b -= 1
    while (a - 1)*b >= A:
        a -= 1
    matrix = create_matrix(a,b)
    while score_matrix(matrix) < a * b:
        i, j = best_point(matrix, a, b)
        print(i + 1, j + 1)
        stdout.flush()
        i, j = tuple(int(x) - 1 for x in input().split(' '))
        if i == -1 and j == -1:
            return
        if i == -2 and j == -2:
            return
        matrix[i][j] = 1
    return

def main():
    T = int(input())
    for _ in range(T):
        A = int(input())
        handle_case(A)

main()