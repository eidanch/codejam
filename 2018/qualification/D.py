
from collections import namedtuple

class InputVars(object):
    def __init__(self, names, dtypes=None):
        self.names = names
        if dtypes is None:
            dtypes = len(names)*[int]
        self.dtypes = dtypes
    
    def __call__(self, vars):
        s = input()
        result = {}
        for name, dtype, sval in zip(self.names, self.dtypes, s.split(' ')):
            result[name] = dtype(sval)
        return result

class InputList(object):
    def __init__(self, name, dtype=None):
        self.name = name
        if dtype is None:
            dtype = int
        self.dtype = dtype
    
    def __call__(self, vars):
        s = input()
        return { self.name: [self.dtype(w) for w in s.split(' ')] }

class InputLines(object):
    def __init__(self, name, count_var, line_input):
        self.name = name
        self.count_var = count_var
        self.line_input = line_input
    
    def __call__(self, vars):
        count = vars[self.count_var]
        result = []
        for i in range(count):
            d = self.line_input(vars)
            value = namedtuple("line", d.keys())(**d)
            result.append(value)
        return { self.name: result }

class CodeJamSolver(object):
    def __init__(self, inputs, solve, output_formatter=None):
        self.inputs = inputs
        self.solve = solve
        if output_formatter is None:
            output_formatter = str
        self.output_formatter = output_formatter

    def handle_inputs(self):
        vars = {}
        for inp in self.inputs:
            vars.update(inp(vars))
        return vars

    def handle_case(self, case_id):
        vars = self.handle_inputs()
        result = self.solve(**vars)
        print("Case #{}: {}".format(case_id, self.output_formatter(result)))

    def main(self):
        T = int(input())
        for case_id in range(1, T + 1):
            self.handle_case(case_id)

    def __call__(self):
        self.main()

list_formatter = lambda lst: ' '.join(str(x) for x in lst)

##########################
#       Solve Area       #
##########################

from math import sin, cos, sqrt, pi

def rotate_z(v, theta):
    x,y,z = v
    return (cos(theta)*x - sin(theta)*y, sin(theta)*x + cos(theta)*y, z)

D = [
    InputVars(['A'], [float]),
]

def dist(u,v):
    return sqrt(sum((b - a)**2 for a,b in zip(u,v)))

def rect_area(v00,v01,v10,v11):
    return dist(v00, v01)*dist(v00,v10)

def calc_area(v010,v011,v100,v101):
    x00,_,z00 = v010
    x01,_,z01 = v011
    x10,_,z10 = v100
    x11,_,z11 = v101
    return rect_area((x00,z00), (x01,z01), (x10,z10), (x11,z11))

def find_theta(area, a=0 , b=pi):
    v010,v011,v100,v101 = (-0.5, 0.5, -0.5), (-0.5, 0.5, 0.5), (0.5, -0.5, -0.5), (0.5, -0.5, 0.5)
    theta = (a + b) / 2
    u,v,x,w = rotate_z(v010, theta),rotate_z(v011, theta),rotate_z(v100, theta),rotate_z(v101, theta)
    while abs(area - calc_area(u,v,x,w)) >= 1e-8:
        if calc_area(u,v,x,w) < area:
            b = theta
        else:
            a = theta
        theta = (a + b) / 2
        u,v,x,w = rotate_z(v010, theta),rotate_z(v011, theta),rotate_z(v100, theta),rotate_z(v101, theta)
    return theta

def solveD(A):
    if A <= 1.414213:
        theta = find_theta(A)
        return (rotate_z((0.5,0,0), theta), rotate_z((0,0.5,0), theta), rotate_z((0,0,0.5), theta))

def D_output_formatter(f):
    u,v,w = f
    return '\n{}\n{}\n{}'.format(' '.join(map(str, u)),' '.join(map(str, v)),' '.join(map(str, w)))


CodeJamSolver(D, solveD, D_output_formatter).main()
