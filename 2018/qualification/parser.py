from collections import namedtuple

class InputVars(object):
    def __init__(self, names, dtypes=None):
        self.names = names
        if dtypes is None:
            dtypes = len(names)*[int]
        self.dtypes = dtypes
    
    def __call__(self, vars):
        s = input()
        result = {}
        for name, dtype, sval in zip(self.names, self.dtypes, s.split(' ')):
            result[name] = dtype(sval)
        return result

class InputList(object):
    def __init__(self, name, dtype=None):
        self.name = name
        if dtype is None:
            dtype = int
        self.dtype = dtype
    
    def __call__(self, vars):
        s = input()
        return { self.name: [self.dtype(w) for w in s.split(' ')] }

class InputLines(object):
    def __init__(self, name, count_var, line_input):
        self.name = name
        self.count_var = count_var
        self.line_input = line_input
    
    def __call__(self, vars):
        count = vars[self.count_var]
        result = []
        for i in range(count):
            d = self.line_input(vars)
            value = namedtuple("line", d.keys())(**d)
            result.append(value)
        return { self.name: result }

class CodeJamSolver(object):
    def __init__(self, inputs, solve, output_formatter=None):
        self.inputs = inputs
        self.solve = solve
        if output_formatter is None:
            output_formatter = str
        self.output_formatter = output_formatter

    def handle_inputs(self):
        vars = {}
        for inp in self.inputs:
            vars.update(inp(vars))
        return vars

    def handle_case(self, case_id):
        vars = self.handle_inputs()
        result = self.solve(**vars)
        print("Case #{}: {}".format(case_id, self.output_formatter(result)))

    def main(self):
        T = int(input())
        for case_id in range(1, T + 1):
            self.handle_case(case_id)

    def __call__(self):
        self.main()

list_formatter = lambda lst: ' '.join(str(x) for x in lst)

##########################
#       Test Area        #
##########################

B = [
    InputVars(['N']),
    InputList('P')
]

C = [
    InputVars(['D', 'N']),
    InputLines('horses', 'N', InputVars(['K', 'S']))
]

D = [
    InputVars(['N', 'K']),
]

def solveB(N, P):
    from string import ascii_uppercase
    parties = {}
    result = []
    for i in range(len(P)):
        parties[ascii_uppercase[i]] = P[i]
    while sum(parties.values()) > 0:
        e1 = max(parties.keys(), key=parties.get)
        parties[e1] -= 1
        if max(parties.values()) > sum(parties.values()) / 2:
            e2 = max(parties.keys(), key=parties.get)
            parties[e2] -= 1
        else:
            e2 = ''
        result.append(e1 + e2)
    return result

def solveC(N, D, horses):
    result = None
    for K, S in horses:
        time = (D - K) / S
        speed = D / time
        if result is None:
            result = speed
        else:
            result = min(result, speed)
    return result

def divide(x):
    from math import ceil, floor
    half = (x - 1)/2
    return ceil(half), floor(half)

def solveD(N, K):
    from collections import defaultdict
    counts = defaultdict(lambda: 0)
    counts[N] += 1
    while K > 0:
        m = max(counts)
        c = counts[m]
        r,l  = divide(m)
        if c >= K:
            return max(l,r), min(l,r)
        K -= c
        counts[r] += c
        counts[l] += c
        del counts[m]
    return None

CodeJamSolver(D, solveD, list_formatter).main()