import sys
sys.path.append('../../')

from tools.problem import *

class AllYourBaseProblem(CodeJamProblem):
    def case_description(self):
        return [formatString]

    def handle_case(self,case):
        line = case[0].rstrip()
        digits = set(range(0,9))
        trans = dict()
        t = 0
        for i in range(len(line)):
            if line[i] in trans.keys():
                continue
            if t == 0:
                s = 1
            elif t == 1:
                s = 0
            else:
                s = t
            trans[line[i]] = s
            t += 1
        base = max(t,2)
        return str(sum(trans[line[i]]*pow(base,len(line) - i - 1) for i in range(len(line))))
            
    

if __name__ == '__main__':
    mainproblem(AllYourBaseProblem())
    