import sys
sys.path.append('../../')
from tools.problem import *
from collections import deque


def handleCase(case):
    E = dict()
    C = dict()
    for (a,b) in case:
        if a not in E.keys():
            E[a] = set()
        if b not in E.keys():
            E[b] = set()
        E[a].add(b)
        E[b].add(a)
    while set(E.keys()) != set(C.keys()):
        q = deque()
        v = (set(E.keys()) - set(C.keys())).pop()
        C[v] = 0
        q.append(v)
        while len(q) > 0:
            v = q.popleft()
            for u in E[v]:
                if u not in C.keys():
                    C[u] = 1 - C[v]
                    q.append(u)
                elif C[u] == C[v]:
                    return 'No'
    return 'Yes'
        
    
def input():
    count = int(raw_input())
    for i in range(count):
        N = int(raw_input())
        E = list()
        for j in range(N):
            E.append(tuple(formatStringList(raw_input().rstrip())))
        yield (i+1,E)
        
def mainproblem():
    for i,case in input():
        print "Case #%d: %s" %(i,handleCase(case))
        
if __name__ == '__main__':
    mainproblem()