import sys
sys.path.append('../../')
from tools.problem import *

class FileFixItProblem(CodeJamProblem):
    def case_description(self):
        return None

    def handle_case(self,case):
        N,M,exist_paths,create_paths = case[0], case[1], case[2], case[3]
        createdirs = set()
        for path in create_paths:
            broken_path = path[1:].rstrip().split('/')
            for i in range(1,len(broken_path)+1):
                createdirs.add('/' + '/'.join(broken_path[:i]))
        existdirs = set()
        for path in exist_paths:
            broken_path = path[1:].rstrip().split('\\')
            for i in range(1,len(broken_path)+1):
                existdirs.add('/' + '/'.join(broken_path[:i]))
        return str(len(createdirs - existdirs))

def input(problem):
    count = int(raw_input())
    for i in range(count):
        NM = formatIntegerList(raw_input())
        N,M = NM[0],NM[1]
        exist_paths = list()
        for j in range(N):
            exist_paths.append(raw_input())
        create_paths = list()
        for j in range(M):
            create_paths.append(raw_input())
        yield (i+1,(N,M,exist_paths,create_paths))
        
def mainproblem(problem):
    for i,case in input(problem):
        print "Case #%d: %s" %(i,problem.handle_case(case))

if __name__ == '__main__':
    mainproblem(FileFixItProblem())