
def input():
    count = int(raw_input())
    for i in range(count):
        vlen = int(raw_input())
        v1 = map(int, raw_input().split(' '))
        v2 = map(int, raw_input().split(' '))
        yield i+1,v1,v2
        
def msp(v1,v2):
    v1.sort()
    v2.sort(reverse=True)
    return sum(v1[i]*v2[i] for i in range(len(v1)))
    
def main():
    for i,v1,v2 in input():
        print "Case #%d: %d" %(i,msp(v1,v2))

if __name__ == '__main__':
    main()