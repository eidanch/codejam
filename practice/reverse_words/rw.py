
def input():
    count = int(raw_input())
    for i in range(count):
        line = raw_input()
        yield i+1,line
        
def rw(line):
    return ' '.join(reversed(line.rstrip().split(' ')))
    
def main():
    for i,line in input():
        print "Case #%d: %s" %(i,rw(line))

if __name__ == '__main__':
    main()