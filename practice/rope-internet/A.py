import sys
sys.path.append('../../')
from tools.problem import *

class RopeInternetProblem(CodeJamProblem):
    def case_description(self):
        return [formatInteger,formatIntegerList,formatIntegerList]

    def handle_case(self,case):
        N,AB = case[0], case[1]
        result = 0
        for i in range(N):
            ai,bi = AB[i]
            result += len(filter(lambda (a,b): a > ai and b < bi or a < ai and b > bi, AB))
        return str(result // 2)

def input(problem):
    count = int(raw_input())
    for i in range(count):
        N = int(raw_input())
        AB = list()
        for j in range(N):
            AB.append(tuple(formatIntegerList(raw_input())))
        yield (i+1,(N,AB))
        
def mainproblem(problem):
    for i,case in input(problem):
        print "Case #%d: %s" %(i,problem.handle_case(case))

if __name__ == '__main__':
    mainproblem(RopeInternetProblem())