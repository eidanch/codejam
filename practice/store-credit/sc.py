import sys
sys.path.append('../../')

from tools.problem import *

class StoreCreditProblem(CodeJamProblem):
    def case_description(self):
        return [formatInteger,formatInteger,formatIntegerList]

    def handle_case(self,case):
        C,I,L = case[0], case[1],case[2]
        for i in range(I):
            if 2*L[i] == C:
                tmplst = L[i+1:]
                if L[i] in tmplst:
                    return outFormatIntegerList((i+1,i + 2 + tmplst.index(C - L[i])))
            elif C - L[i] in L:
                return outFormatIntegerList((i+1,L.index(C - L[i])+1))
    

if __name__ == '__main__':
    mainproblem(StoreCreditProblem())
    