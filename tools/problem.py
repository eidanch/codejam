
class CodeJamProblem(object):
    def case_description(self):
        raise NotImplementedError()

    def handle_case(self, case):
        raise NotImplementedError()

formatInteger = lambda s: int(s)
formatString = lambda s: s
formatIntegerList = lambda s: list(map(int,s.split(' ')))
formatStringList = lambda s: list(s.split(' '))

outFormatIntegerList = lambda intlst: ' '.join(map(str,intlst))


def standard_input(problem):
    count = int(raw_input())
    for i in range(count):
        case = list()
        for frmt in problem.case_description():
            case.append(frmt(raw_input()))
        yield (i+1,case)


def main_problem(problem):
    for i,case in standard_input(problem):
        print "Case #%d: %s" % (i, problem.handle_case(case))
